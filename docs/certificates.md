# Certificate import example
The role allows control over imported certificates and private keys
if delete_all_certificates us true - deletes all CRLs, public and private keys.

Example usage:

```
mikrotik_tls_certificates:
  delete_all_certificates: <yes|no> # if true - deletes ALL certificates and CRLs
  delete_configs_on_target_router: <yes|no> # if true - deletes keys and configs from target router
  certificates:
    - name: certificate 1 # Required. Name for a certificate
      public_key_source_path: path_1 # Required. path on control PC to copy from
      private_key_source_path: path_2 # Default - empty. path on control PC to copy from. If not set - not import any private key.
      private_key_password: password # Password to decrypt private key
      trusted: <yes|no> # Required. Set trusted or not

  crls:
    - url: "http://example.com/crl.pem" # path to download CRL from
```
